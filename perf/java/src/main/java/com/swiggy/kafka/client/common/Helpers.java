package com.swiggy.kafka.client.common;

import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.consumer.ConsumerEnv;
import com.swiggy.kafka.client.producer.ProducerEnv;
import com.swiggy.kafka.clients.configs.*;
import com.swiggy.kafka.clients.configs.enums.ConsumerAutoOffsetReset;
import com.swiggy.kafka.clients.configs.enums.ProducerAcks;

import java.util.HashMap;
import java.util.Map;

public class Helpers {

    public static Topic buildTopic() {
        //Building the Topic
        String topicName = Application.argumentMap.get(Env.TOPIC_NAME);
        Topic.TopicBuilder topicBuilder = Topic.builder().name(topicName);

        if(Boolean.parseBoolean(Application.argumentMap.get(Env.FAULT_STRATEGY))) {
            topicBuilder = topicBuilder.faultStrategy(Topic.FaultStrategy.DUAL_RW);
        } else {
            topicBuilder = topicBuilder.faultStrategy(Topic.FaultStrategy.NONE);
        }

        return topicBuilder.build();
    }

    public static CommonConfig.Cluster buildPrimaryCluster() {
        CommonConfig.Cluster.ClusterBuilder primaryBuilder = null;
        if (!Application.argumentMap.get(Env.PRIMARY_BOOTSTRAPSERVER).equals("")) {
            primaryBuilder = CommonConfig.Cluster.builder().bootstrapServers(Application.argumentMap.get(Env.PRIMARY_BOOTSTRAPSERVER));

            if (Boolean.parseBoolean(Application.argumentMap.get(Env.PRIMARY_AUTH))) {
                primaryBuilder = primaryBuilder.authMechanism(AuthMechanism.SASL_PLAIN);

                if (!Application.argumentMap.get(Env.PRIMARY_USERNAME).equals("")) {
                    primaryBuilder = primaryBuilder.username(Application.argumentMap.get(Env.PRIMARY_USERNAME));
                }

                if (!Application.argumentMap.get(Env.PRIMARY_PASSWORD).equals("")) {
                    primaryBuilder = primaryBuilder.password(Application.argumentMap.get(Env.PRIMARY_PASSWORD));
                }

            } else {
                primaryBuilder = primaryBuilder.authMechanism(AuthMechanism.NONE);
            }

            return primaryBuilder.build();
        } else {
            return null;
        }
    }

    public static CommonConfig.Cluster buildSecondaryCluster() {
        CommonConfig.Cluster.ClusterBuilder secondaryBuilder = null;
        if (!Application.argumentMap.get(Env.SECONDARY_BOOTSTRAPSERVER).equals("")) {
            secondaryBuilder = CommonConfig.Cluster.builder().bootstrapServers(Application.argumentMap.get(Env.SECONDARY_BOOTSTRAPSERVER));

            if (Boolean.parseBoolean(Application.argumentMap.get(Env.SECONDARY_AUTH))) {
                secondaryBuilder = secondaryBuilder.authMechanism(AuthMechanism.SASL_PLAIN);

                if (!Application.argumentMap.get(Env.SECONDARY_USERNAME).equals("")) {
                    secondaryBuilder = secondaryBuilder.username(Application.argumentMap.get(Env.SECONDARY_USERNAME));
                }

                if (!Application.argumentMap.get(Env.SECONDARY_PASSWORD).equals("")) {
                    secondaryBuilder = secondaryBuilder.password(Application.argumentMap.get(Env.SECONDARY_PASSWORD));
                }

            } else {
                secondaryBuilder = secondaryBuilder.authMechanism(AuthMechanism.NONE);
            }

            return secondaryBuilder.build();
        } else {
            return null;
        }
    }


    public static ProducerConfig buildProducerConfig(Topic topic, CommonConfig.Cluster primaryCluster, CommonConfig.Cluster secondaryCluster) {

        Map<String,Topic> topics = new HashMap<>();
        topics.put(topic.getName(),topic);

        ProducerAcks ack = ProducerAcks.ALL;
        if (Application.argumentMap.get(ProducerEnv.ACK).equals("ONE")) {
            ack = ProducerAcks.ONE;
        } else if (Application.argumentMap.get(ProducerEnv.ACK).equals("NONE")) {
            ack = ProducerAcks.NONE;
        }

        boolean isCompressed = true;
        if (!Boolean.parseBoolean(Application.argumentMap.get(ProducerEnv.IS_COMPRESSED))) {
            isCompressed = false;
        }

        ProducerConfig.ProducerConfigBuilder producerConfigBuilder = ProducerConfig.builder()
                .primary(primaryCluster)
                .topics(topics)
                .acks(ack)
                .enableCompression(isCompressed)
                .clientId(Application.argumentMap.get(ProducerEnv.PRODUCER_CLIENT_ID))
                .retries(Integer.parseInt(Application.argumentMap.get(ProducerEnv.NO_OF_RETRIES)));

        if (secondaryCluster!=null) {
            producerConfigBuilder.secondary(secondaryCluster);
        }

        return producerConfigBuilder.build();
    }

    public static ConsumerConfig buildConsumerConfig(Topic topic, CommonConfig.Cluster primaryCluster, CommonConfig.Cluster secondaryCluster) {

        Map<String,Topic> topics = new HashMap<>();
        topics.put(topic.getName(),topic);

        ConsumerAutoOffsetReset autoOffsetReset = ConsumerAutoOffsetReset.LATEST;

        if (!Boolean.parseBoolean(Application.argumentMap.get(ConsumerEnv.IS_LATEST_OFFSET))) {
            autoOffsetReset = ConsumerAutoOffsetReset.EARLIEST;
        }

        boolean isReplicateOffset = true;

        if (!Boolean.parseBoolean(Application.argumentMap.get(ConsumerEnv.IS_INTERCEPTOR_ENABLED))) {
            isReplicateOffset = false;
        }

        ConsumerConfig.ConsumerConfigBuilder consumerConfigBuilder = ConsumerConfig.builder()
                .primary(primaryCluster)
                .consumerGroupId(Application.argumentMap.get(ConsumerEnv.CONSUMER_GROUP_ID))
                .topic(topic)
                .autoOffsetResetConfig(autoOffsetReset)
                .concurrency(Integer.parseInt(Application.argumentMap.get(ConsumerEnv.CONCURRENCY)))
                .maxPollRecords(Integer.parseInt(Application.argumentMap.get(ConsumerEnv.MAX_POLL_RECORDS)))
                .offsetReplication(isReplicateOffset)
                .clientId(Application.argumentMap.get(ConsumerEnv.CONSUMER_CLIENT_ID));

        if (secondaryCluster != null) {
            consumerConfigBuilder.secondary(secondaryCluster);
        }


        return consumerConfigBuilder.build();
    }
}
