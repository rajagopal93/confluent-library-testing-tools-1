package com.swiggy.kafka.client.pojo;


public class Message {

        private final String id;
        private final long timestamp;
        private final String data;

        private Message(MessageBuilder builder) {
            this.id = builder.id ;
            this.timestamp = builder.timestamp;
            this.data = builder.data;
        }

        public static MessageBuilder builder() {
            return new MessageBuilder();
        }

        public long getTimeStamp() {
            return this.timestamp;
        }

        public static class MessageBuilder {
            private String id;
            private long timestamp;
            private String data;


            public MessageBuilder id(String id) {
                this.id = id;
                return this;
            }

            public MessageBuilder data(String data) {
                this.data = data;
                return this;
            }

            public MessageBuilder timestamp(long timestamp) {
                this.timestamp = timestamp;
                return this;
            }

            public Message build() {
                return new Message(this);
            }
        }

}
