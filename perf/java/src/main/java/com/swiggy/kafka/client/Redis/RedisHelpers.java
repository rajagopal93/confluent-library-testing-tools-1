package com.swiggy.kafka.client.Redis;

import com.swiggy.kafka.client.Application;
import com.swiggy.kafka.client.common.Env;
import com.swiggy.kafka.client.consumer.SampleConsumer;
import redis.clients.jedis.Jedis;

public class RedisHelpers {


    public synchronized static void updateConsumerResultsInRedis(String id) {
        if (Boolean.parseBoolean(Application.argumentMap.get(Env.IS_REDIS_ENABLED))) {
            Jedis conn = RedisClient.getInstance(Application.argumentMap.get(Env.REDIS_HOST), Integer.parseInt(Application.argumentMap.get(Env.REDIS_PORT))).getResource();
            long result = conn.sadd("CONSUMED_MESSAGE", id);
            conn.close();
            if (result == 0) {
                SampleConsumer.duplicateCount.addAndGet(1);
            }
        }
    }
}
