package main
import (
	"bitbucket.org/swigy/kafka-client-go/swgykafka"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"
)
var cprimary *swgykafka.Cluster
var csecondary *swgykafka.Cluster
var consumer *swgykafka.Consumer
var cconfig *swgykafka.ConsumerConfig
var wgConsumer sync.WaitGroup
var consumerThreads = 1
var isLatestOffset = true
var maxPollRecords = 10
var autoResetConfig = swgykafka.Latest
var processedCount int64
var processingErrorCount int64
var rateLimiter <-chan time.Time
var consumerDuration = 60
var autoCommitInterval = 100
var consumerClientId = "algates-consumer"
var consumerGroupId = "algates-consumer-group-8"
var isAutoCommitEnabled = false
var isConsumerDebug = false
var consumerTopicName = "perftest_confluent"
var consumedCountChannel = make(chan int, 1000000)
var wgConsumedCount sync.WaitGroup
var expectedConsumedMessageCount = -1
var tolerancePercentage_consumer = 10
var faultStrategy_consumer = "FaultNone"
var cfaultdualsetup = "PrimaryUpSecondaryDown"
func main() {
	bootstrapServer := "pkc-lg991.us-east-1.aws.confluent.cloud:9092"
	username := "VNA2WFZCDEESAEZO"
	password := "0qOUtoP4UKJ1tdxgzoG71/Wq4jQZ0IOrW/r9I8cgi1z65qzbxja50CT5mf7iGRT1"
	Inhouse_cluster := "central-kafka-cluster-01.swiggyperf.in:9092,central-kafka-cluster-02.swiggyperf.in:9092,central-kafka-cluster-03.swiggyperf.in:9092,central-kafka-cluster-04.swiggyperf.in:9092,central-kafka-cluster-05.swiggyperf.in:9092,central-kafka-cluster-06.swiggyperf.in:9092"
	Inhouse_clusterDown := "central-kafka-cluster-01.swiggyperf.in:9095,central-kafka-cluster-02.swiggyperf.in:9095,central-kafka-cluster-03.swiggyperf.in:9095,central-kafka-cluster-04.swiggyperf.in:9095,central-kafka-cluster-05.swiggyperf.in:9095,central-kafka-cluster-06.swiggyperf.in:9095"
	if len(os.Args) > 1 {
		consumerThreads,_ = strconv.Atoi(os.Args[1])
	}
	if len(os.Args) > 2 {
		isLatestOffset,_ = strconv.ParseBool(os.Args[2])
	}
	if len(os.Args) > 3 {
		consumerDuration,_ = strconv.Atoi(os.Args[3])
	}
	if len(os.Args) > 4 {
		consumerTopicName  = os.Args[4]
	}
	if len(os.Args) > 5 {
		isAutoCommitEnabled,_ = strconv.ParseBool(os.Args[5])
	}
	if len(os.Args) > 6 {
		autoCommitInterval,_ = strconv.Atoi(os.Args[6])
	}
	if len(os.Args) > 7 {
		expectedConsumedMessageCount,_ = strconv.Atoi(os.Args[7])
	}
	if len(os.Args) > 8 {
		tolerancePercentage_consumer,_ = strconv.Atoi(os.Args[8])
	}
	if len(os.Args) > 9 {
		faultStrategy_consumer = os.Args[9]
	}
	if len(os.Args) > 10 {
		isConsumerDebug,_ = strconv.ParseBool(os.Args[10])
	}
	if len(os.Args) > 11{
		faultdualsetup = os.Args[13]
	}
	if !isLatestOffset {
		autoResetConfig = swgykafka.Earliest
	}
	consumerGroupId = consumerTopicName + "_" + consumerGroupId
	consumerClientId = consumerTopicName + "_" + consumerClientId
	if cfaultdualsetup == "PrimaryUpSecondaryUp" {
		cprimary, _ = swgykafka.NewClusterBuilder(bootstrapServer).AuthMechanism(swgykafka.AuthSaslPlain).UserName(username).Password(password).Build()
		csecondary,_ = swgykafka.NewClusterBuilder(Inhouse_cluster).AuthMechanism(swgykafka.AuthNone).Build()
	}else if cfaultdualsetup == "PrimaryUpSecondaryDown"{
		cprimary, _ = swgykafka.NewClusterBuilder(bootstrapServer).AuthMechanism(swgykafka.AuthSaslPlain).UserName(username).Password(password).Build()
		csecondary,_ = swgykafka.NewClusterBuilder(Inhouse_clusterDown).AuthMechanism(swgykafka.AuthNone).Build()
	}else if cfaultdualsetup == "SecondaryUpPrimaryDown"{
		csecondary, _ = swgykafka.NewClusterBuilder(bootstrapServer).AuthMechanism(swgykafka.AuthSaslPlain).UserName(username).Password(password).Build()
		cprimary,_ = swgykafka.NewClusterBuilder(Inhouse_clusterDown).AuthMechanism(swgykafka.AuthNone).Build()
	}else if cfaultdualsetup =="FaultNone" {
		cprimary, _ = swgykafka.NewClusterBuilder(bootstrapServer).AuthMechanism(swgykafka.AuthSaslPlain).UserName(username).Password(password).Build()
	}

	if faultStrategy_consumer == "FaultNone"{
		topic, _ := swgykafka.NewTopicBuilder(consumerTopicName).FaultStrategy(swgykafka.FaultDualRW).Build()
		cconfig, _ = swgykafka.NewConsumerConfigBuilder(primary, nil, consumerClientId,topic,consumerGroupId).Concurrency(consumerThreads).AutoOffsetReset(autoResetConfig).EnableAutoCommit(isAutoCommitEnabled).AutoCommitIntervalMs(autoCommitInterval).Build()

	}else{
		topic, _ := swgykafka.NewTopicBuilder(consumerTopicName).FaultStrategy(swgykafka.FaultDualRW).Build()
		cconfig, _ = swgykafka.NewConsumerConfigBuilder(primary, secondary, consumerClientId,topic,consumerGroupId).Concurrency(consumerThreads).AutoOffsetReset(autoResetConfig).EnableAutoCommit(isAutoCommitEnabled).AutoCommitIntervalMs(autoCommitInterval).Build()

	}

	var err error
	consumer, err = swgykafka.NewConsumer(*cconfig,MyHandler{},nil)
	if err != nil {
		panic(err)
	}
	err = consumer.Start()
	if err != nil {
		fmt.Printf("error occorred in starting consumer : %v", err)
	}
	go consumedCountWorker()
	var now = time.Now()
	var future = time.Now().Add(time.Second * time.Duration(consumerDuration))
	for now.Unix() < future.Unix() {
		now = time.Now()
	}
	wgConsumedCount.Wait()

	toleranceValue_consumer := float64(tolerancePercentage_consumer)/float64(100)
	expectedConsumedMessageCount_tolerancevalue := float64(expectedConsumedMessageCount) - toleranceValue_consumer*float64(expectedConsumedMessageCount)

	totalConsumerSuccessCount := strconv.FormatInt(processedCount/int64(consumerDuration),10)
	fmt.Println("TotalCount Consumed Per Sec -->" + totalConsumerSuccessCount)
	if expectedConsumedMessageCount != -1 {
		if ((processedCount/int64(consumerDuration)) > int64(expectedConsumedMessageCount) || (processedCount/int64(consumerDuration)) >= int64(expectedConsumedMessageCount_tolerancevalue)) {
			fmt.Println("Testcase: PASSED")
		}else {
			fmt.Println("Testcase: FAILED")
		}
	} else{
		fmt.Println("Test case execution is completed!!!")
	}

}
type MyHandler struct {
}
func (m MyHandler) Handle(record *swgykafka.Record) (swgykafka.Status, error) {
	wgConsumedCount.Add(1)
	consumedCountChannel <- 1
	return swgykafka.Success, nil
}
func consumedCountWorker() {
	for {
		<-  consumedCountChannel
		processedCount += 1
		wgConsumedCount.Done()
	}
}
